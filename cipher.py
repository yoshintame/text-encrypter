import argparse
import base64
from binascii import Error

import cryptography
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC


def write_key():
    key = Fernet.generate_key()
    print(key)


def passkey(password):
    password = password.encode()

    mysalt = b'\xba\x95\xdb\xfeOX\x11\x81\xdbh\xad\x15\xb0\x18)\x98'

    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256,
        length=32,
        salt=mysalt,
        iterations=10000,
        backend=default_backend()
    )

    key = base64.urlsafe_b64encode(kdf.derive(password))
    return key


def encrypt(decrypted_string, key):
    f = Fernet(key)
    encrypted_data = f.encrypt(decrypted_string.encode())
    print(encrypted_data)
    encrypted_string = encrypted_data.decode()
    return encrypted_string


def decrypt(encrypted_string, key):
    f = Fernet(key)
    try:
        decrypted_data = f.decrypt(encrypted_string)
        return decrypted_data.decode()
    except (cryptography.fernet.InvalidToken, TypeError, Error) as error:
        return "Invalid token"


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Encrypt and decrypt files using password as key')
    parser.add_argument(
        'file',
        type=str,
        help='provide the path to the file to be encrypted or decrypted'
    )
    parser.add_argument(
        'operation',
        type=str,
        help='operation: encrypt (e) or decrypt (d)'
    )
    args = parser.parse_args()

    if args.operation == "e":
        key = passkey("6255921")
        print(encrypt("text", key))
    elif args.operation == "d":
        key = passkey("6255921")
        print(decrypt('gAAAAABjiIzy1Dp_SUhYrK5ea7Ll9ZgfHWuvqSCJUH9q9OugsZjPg41quis3LlaoYy7giU5XqwE8-5R00y3zFWg-h4I397g-9Q=', key))
    else:
        print("Error: unknown operation argument. (use --help for description)")

