import sys

from PyQt5.QtWidgets import QTextEdit, QMainWindow, QApplication, QPushButton, QLineEdit, QMessageBox

from cipher import passkey, encrypt, decrypt


class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.encrypted_textbox = None
        self.password_button = None
        self.password_box = None
        self.decrypted_textbox = None
        self.key = None
        self.password = ""
        self.title = 'Ksyuton encrypter'
        self.left = 100
        self.top = 100
        self.width = 320
        self.height = 390
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.password_box = QLineEdit(self)
        self.password_box.setPlaceholderText("Password...")
        self.password_box.setEchoMode(QLineEdit.Password)
        self.password_box.move(20, 21)
        self.password_box.resize(230, 28)

        self.password_button = QPushButton(self)
        self.password_button.move(260, 20)
        self.password_button.resize(40, 30)
        self.password_button.setText("Ok")
        self.password_button.clicked.connect(self.password_entered)

        self.decrypted_textbox = QTextEdit(self)
        self.decrypted_textbox.setPlaceholderText("Decrypted text")
        self.decrypted_textbox.move(20, 60)
        self.decrypted_textbox.resize(280, 200)
        self.decrypted_textbox.textChanged.connect(self.encrypt_textbox)

        self.encrypted_textbox = QTextEdit(self)
        self.encrypted_textbox.setPlaceholderText("Encrypted token")
        self.encrypted_textbox.move(20, 270)
        self.encrypted_textbox.resize(280, 100)
        self.encrypted_textbox.textChanged.connect(self.decrypt_textbox)

        self.show()

    def password_required(self):
        QMessageBox.question(self, 'Error message', "Password required", QMessageBox.Ok, QMessageBox.Ok)

    def password_entered(self):
        self.password = self.password_box.text()
        self.key = passkey(self.password)
        self.password_box.setText("")

    def encrypt_textbox(self):
        if self.password == "":
            self.password_required()
            block_signals_and_set_text("", [self.encrypted_textbox, self.decrypted_textbox])
            return

        encrypted_message = encrypt(str(self.decrypted_textbox.toPlainText()), self.key)
        block_signals_and_set_text(encrypted_message, [self.encrypted_textbox, self.decrypted_textbox])

    def decrypt_textbox(self):
        if self.password == "":
            self.password_required()
            block_signals_and_set_text("", [self.decrypted_textbox, self.encrypted_textbox])
            return

        decrypted_message = decrypt(self.encrypted_textbox.toPlainText(), self.key)
        block_signals_and_set_text(decrypted_message, [self.decrypted_textbox, self.encrypted_textbox])


def block_signals_and_set_text(text, textboxes):
    for textbox in textboxes:
        textbox.blockSignals(True)
    textboxes[0].setText(text)
    for textbox in textboxes:
        textbox.blockSignals(False)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
